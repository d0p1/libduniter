#ifndef DUNITER_ERROR_H
# define DUNITER_ERROR_H 1

/**
 * \ingroup libduniter
 * \{
 */

enum duniter_error {
	DUNITER_OK = 0,
	DUNITER_ERR = -1,
	DUNITER_ERR_SS58_INVALID_PREFIX = -2,

	DUNITER_ERR_NOT_IMPLEM = -666,
};

const char *duniter_strerror(enum duniter_error);

/** \} */

#endif /* DUNITER_ERROR_H */