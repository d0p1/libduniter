#ifndef DUNITER_RPC_RESPONSE_H
# define DUNITER_RPC_RESPONSE_H 1

enum duniter_rpc_response_type {
	DUNITER_RPC_ERROR,
	DUNITER_RPC_STRING,
	DUNITER_RPC_OBJECT
};

struct duniter_rpc_response {
	enum duniter_rpc_response_type type;
	union {
		char *string;
	};
};

#endif /* !DUNITER_RPC_RESPONSE_H */