#ifndef DUNITER_RPC_NODE_H
# define DUNITER_RPC_NODE_H

# include <curl/curl.h>

struct duniter_node {
	CURL *curl;
};

int duniter_node_connect(struct duniter_node *node, const char *url);
void duniter_node_close(struct duniter_node *node);
void duniter_rpc_loop(struct duniter_node *node);

#endif /* !DUNITER_RPC_NODE_H */