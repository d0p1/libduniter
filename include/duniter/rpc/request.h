#ifndef DUNITER_RPC_REQUEST_H
# define DUNITER_RPC_REQUEST_H 1

#include <duniter/rpc/response.h>

enum duniter_rpc_request_type {
	DUNITER_RPC_chain_getBlock,
	DUNITER_RPC_system_chain,
	DUNITER_RPC_system_chainType,
	DUNITER_RPC_system_health,
	DUNITER_RPC_system_name,
	DUNITER_RPC_system_version,
};

struct duniter_rpc_request {
	enum duniter_rpc_request_type type;
	union {
		struct {
			char *BlockHash;
		} chain_getBlock;
	};
};

struct duniter_rpc_request *duniter_rpc_request_new(enum duniter_rpc_request_type type, ...);
void duniter_rpc_request_destroy(struct duniter_rpc_request *req);

struct duniter_rpc_response *duniter_rpc_execute(const char *url, struct duniter_rpc_request *req);
void duniter_rpc_response_destroy(struct duniter_rpc_request *req);

#endif /* DUNITER_RPC_REQUEST_H */