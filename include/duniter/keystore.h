#ifndef DUNITER_KEYSTORE_H
# define DUNITER_KEYSTORE_H 1

# include <duniter/account/keypair.h>

/**
 * \ingroup keystore
 * \{
 */

struct duniter_keystore {
	uint8_t type;
	uint8_t seed[DUNITER_SEED_BYTES];
} __attribute__((packed));

int duniter_keystore_from_file(struct duniter_keystore *ks, const char *path);
int duniter_keystore_save_to_file(struct duniter_keystore *ks, const char *path);

/** \} */

#endif /* !DUNITER_KEYSTORE_H */