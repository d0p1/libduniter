#ifndef DUNITER_ACCOUNT_KEYPAIR_H
# define DUNITER_ACCOUNT_KEYPAIR_H 1

# include <stdint.h>

/**
 * \ingroup account
 * \{
 */

# define DUNITER_SEED_BYTES      32U
# define DUNITER_PUBLICKEY_BYTES 32U
# define DUNITER_SECRETKEY_BYTES (32U + 32U)

enum duniter_keypair_type {
	DUNITER_ED25519,
	DUNITER_SR25519
};

struct duniter_keypair {
	enum duniter_keypair_type type;
	uint8_t pk[DUNITER_PUBLICKEY_BYTES]; /**< Public key */
	uint8_t sk[DUNITER_SECRETKEY_BYTES]; /**< Secret key */
};

/**
 * Generate ed25519 key pair from seed
 * \param[out] kp
 * \param[in] seed
 *
 * \sa duniter_keypair DUNITER_SEED_BYTES
 */
int duniter_keypair_from_seed(struct duniter_keypair *kp, const uint8_t seed[DUNITER_SEED_BYTES],
	enum duniter_keypair_type type);

/**
 * Generate ed25519 key pair from secret key
 * \param[out] kp
 * \param[in] sk
 */
int duniter_keypair_from_sk(struct duniter_keypair *kp, const uint8_t sk[DUNITER_SECRETKEY_BYTES],
	enum duniter_keypair_type type);

/** \} */

#endif /* DUNITER_ACCOUNT_KEYPAIR_H */