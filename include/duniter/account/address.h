#ifndef DUNITER_ACCOUNT_ADDRESS_H
# define DUNITER_ACCOUNT_ADDRESS_H 1

# include <duniter/account/keypair.h>

/**
 * \ingroup account
 * \{
 */

# define DUNITER_ADDRESS_LENGTH 49

enum address_type {
	DUNITER_ADDRESS_LEGACY, /* duniter v1 */
	DUNITER_ADDRESS_SS58,   /* duniter v2 */
};

/**
 * \param[out] pk 
 * \param[in] address
 */
int duniter_address_to_pubkey(uint8_t pk[DUNITER_PUBLICKEY_BYTES], const char *address);

/**
 * \param[out] address
 * \param[in] network
 * \param[in] pk
 */
int duniter_address_from_pubkey(char *address, uint16_t network, uint8_t pk[DUNITER_PUBLICKEY_BYTES]);

/**
 * \}
 */

#endif /* !DUNITER_ACCOUNT_ADDRESS_H */