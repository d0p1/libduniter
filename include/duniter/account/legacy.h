#ifndef DUNITER_ACCOUNT_LEGACY_H
# define DUNITER_ACCOUNT_LEGACY_H 1

# include <stddef.h>
# include <stdint.h>
# include <duniter/account/keypair.h>

/**
 * \ingroup account
 * \{
 */

# define DUNITER_LEGACY_ADDRESS_LENGTH             45
# define DUNITER_LEGACY_ADDRESS_WITH_CHKSUM_LENGTH (DUNITER_LEGACY_ADDRESS_LENGTH + 1 + 3) 

/**
 * \param[out] seed
 * \param[in] password
 * \param[in] salt
 */
int duniter_legacy_seed_from_passphrase(uint8_t seed[DUNITER_SEED_BYTES], const char *password, const char *salt);

/**
 * \param[out] address
 * \param[in] size
 * \param[in] pk
 */
int duniter_legacy_address_from_pubkey(char *address, size_t size, uint8_t pk[DUNITER_PUBLICKEY_BYTES]);

/**
 * \param[out] pk
 * \param[in] address
 */
int duniter_legacy_address_to_pubkey(uint8_t pk[DUNITER_PUBLICKEY_BYTES], const char *address);

/** \} */

#endif /* !DUNITER_ACCOUNT_LEGACY_H */