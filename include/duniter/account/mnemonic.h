#ifndef DUNITER_MNEMONIC_H
# define DUNITER_MNEMONIC_H 1

# include <stddef.h>
# include <stdint.h>
# include <duniter/account/keypair.h>

/**
 * \ingroup account
 * \{
 */

# define DUNITER_MNEMONIC_WORDS         12
# define DUNITER_MNEMONIC_ENTROPY_BYTES 32

enum duniter_mnemonic_lang {
	DUNITER_MNEMONIC_ENGLISH,
	DUNITER_MNEMONIC_FRENCH,
	DUNITER_MNEMONIC_SPANISH,

	DUNITER_MNEMONIC_LANG_MAX
};

int duniter_mnemonic_from_entropy(const char *mnemonic[DUNITER_MNEMONIC_WORDS], enum duniter_mnemonic_lang lang, uint8_t *entropy);
int duniter_mnemonic_generate(const char *mnemonic[DUNITER_MNEMONIC_WORDS], enum duniter_mnemonic_lang lang);
int duniter_mnemonic_to_seed(uint8_t *seed, size_t size, const char *const *mnemonic, const char *pass);

/** \} */

#endif /* :DUNITER_MNEMONIC_H */