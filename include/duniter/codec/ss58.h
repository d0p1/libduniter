#ifndef DUNITER_SS58_H
# define DUNITER_SS58_H 1 

/**
 * \ingroup codec
 * \{
 */

# include <stddef.h>
# include <stdint.h>

# define DUNITER_SS58_G1_PREFIX    4450U
# define DUNITER_SS58_GDEV_PREFIX  42U
# define DUNITER_SS58_GTEST_PREFIX 42U

int duniter_ss58_encode(char *out, size_t *outsz,
						uint8_t *in, uint8_t insz, uint16_t prefix);

/**
 * \}
 */

#endif /* !DUNITER_SS58_H */