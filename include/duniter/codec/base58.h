#ifndef DUNITER_CODEC_BASE58_H
# define DUNITER_CODEC_BASE58_H 1

/**
 * \ingroup codec
 * \{
 */

# include <stddef.h>

/**
 * \param[out] b58
 * \param[out] b58sz
 * \param[in] data
 * \param[in] binsz
 */
int duniter_base58_encode(char *b58, size_t *b58sz, const void *data, size_t binsz);

/**
 * \}
 */

#endif /* !DUNITER_CODEC_BASE58_H */