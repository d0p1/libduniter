#ifndef DUNITER_H
# define DUNITER_H 1

/**
 * \defgroup libduniter
 * \{
 */

# include <duniter/error.h>

/**
 * \defgroup codec
 */
# include <duniter/codec/base58.h>
# include <duniter/codec/ss58.h>
# include <duniter/codec/scale.h>

/**
 * \defgroup account
 */
# include <duniter/account/address.h>
# include <duniter/account/legacy.h>
# include <duniter/account/mnemonic.h>

/**
 * \defgroup rpc
 */
# include <duniter/rpc.h>

/**
 * \defgroup keystore
 */
# include <duniter/keystore.h>


/**
 * Initialize libduniter
 * \return DUNITER_OK on success 
 */
int duniter_initialize(void);

/**
 * \}
 */

#endif /* !DUNITER_H */