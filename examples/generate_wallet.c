#include "duniter/account/address.h"
#include "duniter/account/keypair.h"
#include <sodium/utils.h>
#include <stdlib.h>
#include <stdio.h>
#include <duniter.h>
#include <sodium.h>

void
display_mnemonic(const char *const *mnemonic)
{
	size_t i;

	printf("\033[1mmnemonic:\033[0m");
	for (i = 0; i < DUNITER_MNEMONIC_WORDS; i++)
	{
		printf(" %s", mnemonic[i]);
	}
	printf("\n");
}

int
main(int argc, char **argv)
{
	const char *mnemonic[DUNITER_MNEMONIC_WORDS];
	uint8_t seed[DUNITER_SEED_BYTES];
	char seed_encoded[DUNITER_SEED_BYTES * 2 + 1];
	char pubkey_encoded[DUNITER_PUBLICKEY_BYTES * 2 + 1];
	char privkey_encoded[DUNITER_SECRETKEY_BYTES * 2 + 1];
	char address[DUNITER_ADDRESS_LENGTH];
	char legacy_address[DUNITER_LEGACY_ADDRESS_WITH_CHKSUM_LENGTH];

	struct duniter_keypair kp;

	(void)argc;
	(void)argv;

	duniter_initialize();

	/* generate mnemonic and then seed from mnemonic */
	duniter_mnemonic_generate(mnemonic, DUNITER_MNEMONIC_ENGLISH);
	display_mnemonic(mnemonic);

	/* get seed from mnemonic */
	duniter_mnemonic_to_seed(seed, DUNITER_SEED_BYTES, mnemonic, "");
	printf("\033[1mseed:\033[0m 0x%s\n", sodium_bin2hex(seed_encoded, DUNITER_SEED_BYTES * 2 + 1, seed, DUNITER_SEED_BYTES));

	/* get keypair */
	duniter_keypair_from_seed(&kp, seed, DUNITER_ED25519);
	printf("\033[1mpubkey:\033[0m 0x%s\n", sodium_bin2hex(pubkey_encoded, DUNITER_PUBLICKEY_BYTES * 2 + 1, kp.pk, DUNITER_PUBLICKEY_BYTES));

	duniter_address_from_pubkey(address, DUNITER_SS58_GDEV_PREFIX, kp.pk);
	printf("\033[1mpubkey SS58:\033[0m %s\n", address);

	printf("\033[1mprivkey:\033[0m 0x%s\n", sodium_bin2hex(privkey_encoded, DUNITER_SECRETKEY_BYTES * 2 + 1, kp.sk, DUNITER_SECRETKEY_BYTES));

	/* keypair to address */
	duniter_legacy_address_from_pubkey(legacy_address, DUNITER_LEGACY_ADDRESS_WITH_CHKSUM_LENGTH, kp.pk);
	printf("\033[1mlegacy address:\033[0m %s\n", legacy_address);

	printf("\033[1maddress:\033[0m %s\n", address);
	return (EXIT_SUCCESS);
}