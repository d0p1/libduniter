#include "duniter/rpc/node.h"
#include <stdlib.h>
#include <stdio.h>

int
main(int argc, char **argv)
{
	struct duniter_node node;

	if (argc < 2)
	{
		fprintf(stderr, "usage: %s url\n", argv[0]);
		return (EXIT_FAILURE);
	}

	if (duniter_node_connect(&node, argv[1]) != 0)
	{
		fprintf(stderr, "Can't connect to %s\n", argv[1]);
	}

	/* TODO */
	duniter_rpc_loop(&node);

	duniter_node_close(&node);

	return (EXIT_SUCCESS);
}