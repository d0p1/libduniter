#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "config.h"
#ifdef HAVE_LIBGEN_H
# include <libgen.h>
#endif
#ifdef _WIN32
# include <shlobj.h>
# include <direct.h>
#else
# include <sys/stat.h>
#endif
#include "gettext.h"

#define _(STRING) gettext(STRING)

static const char *prg_name;
static char path[1024];
static char *rpc = "ws://127.0.0.1:9944";
static char *keystore;

static inline void
usage(int retval)
{
	if (retval == EXIT_FAILURE)
	{
		fprintf(stderr, _("Try '%s -h' for more informations.\n"), prg_name);
	}
	else
	{
		printf(_("Usage: %s [-h] [-k KEYSTORE] [--rpc RPC]\n"), prg_name);
		printf(_("Flags:\n"));
		printf(_("\t-h\tdisplay this help and exit.\n"));
		printf(_("Options:\n"));
		printf(_("\t-k KEYSTORE\t(default: %s)\n"), keystore);
		printf(_("\t--rpc RPC\t(default: %s)\n"), rpc);
	}

	exit(retval);
}

int
main(int argc, char **argv)
{

#ifdef HAVE_LIBGEN_H
	prg_name = basename(argv[0]);
#else
	prg_name = argv[0];
#endif

#if defined ENABLE_NLS && ENABLE_NLS
	setlocale(LC_ALL, "");
	bindtextdomain(PACKAGE, LOCALEDIR);
	textdomain(PACKAGE);
#endif /* defined ENABLE_NLS && ENABLE_NLS */

#ifdef _WIN32
	SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, path);
	strcat(path, "\\.duniter");
	mkdir(path);
	strcat(path, "\\keystore");
#else 
	if (getenv("HOME") != NULL)
	{
		strcpy(path, getenv("HOME"));
	}
	strcat(path, "/.duniter");
	mkdir(path, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	strcat(path, "/keystore");
#endif

	keystore = path;

	while ((argc > 1) && (argv[1][0] == '-'))
	{
		switch (argv[1][1])
		{
			case 'h':
				usage(EXIT_SUCCESS);
				break;
			case 'k':
				argc--;
				argv++;
				if (argc < 2) usage(EXIT_FAILURE);
				keystore = argv[1];
				break;
			default:
				usage(EXIT_FAILURE);
				break;
		}

		argc--;
		argv++;
	}

	/*
	 * TODO: parse line from stdin
	 * TODO: implement cmd: balance, address, ....
	 */

	return (EXIT_SUCCESS);
}