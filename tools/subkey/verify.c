#include <stdlib.h>
#include <stdio.h>
#include <duniter.h>
#include "subkey.h"

static inline void
verify_usage(int retval)
{
	if (retval == EXIT_FAILURE)
	{
		fprintf(stderr, "Try '%s sign -h' for more information.\n", prg_name);
	}
	else
	{
		printf("Usage: %s sign [-h]\n", prg_name);
		printf("Flags:\n");
		printf("\t-h\tdisplay this help and exit.\n");
	}

	exit(retval);
}

int
verify_main(int argc, char *argv[])
{
	while ((argc > 1) && (argv[1][0] == '-'))
	{
		switch (argv[1][1])
		{
			case 'h':
				verify_usage(EXIT_SUCCESS);
				break;
		}
	}
	return (EXIT_SUCCESS);
}