#include "duniter/account/keypair.h"
#include "duniter/account/legacy.h"
#include "duniter/error.h"
#include <sodium/utils.h>
#include <stdlib.h>
#include <stdio.h>
#include <duniter.h>
#include <string.h>
#ifdef WIN32
# include <io.h>
# define F_OK 0
# define access _access
#else
# include <unistd.h>
#endif
#include <errno.h>
#include "subkey.h"

static const char *format = "text";
static const char *network = "gdev";
static uint16_t prefix = DUNITER_SS58_GDEV_PREFIX;
static const char *keystore = NULL;

static void inline
display_mnemonic(const char *const *mnemonic)
{
	size_t i;

	for (i = 0; i < DUNITER_MNEMONIC_WORDS; i++)
	{
		if (i > 0)
		{
			printf(" %s", mnemonic[i]);
		}
		else
		{
			printf("%s", mnemonic[i]);
		}
	}
}

static inline void
set_prefix(void)
{
	if (strcasecmp(network, "g1") == 0)
	{
		prefix = DUNITER_SS58_G1_PREFIX;
	}
	else if (strcasecmp(network, "gdev") == 0)
	{
		prefix = DUNITER_SS58_GDEV_PREFIX;
	}
	else if (strcasecmp(network, "gtest") == 0)
	{
		prefix = DUNITER_SS58_GTEST_PREFIX;
	}
	else
	{
		fprintf(stderr, "%s: unknown network, default to gdev\n", network);
	}
}

static inline void
generate_usage(int retval)
{
	if (retval == EXIT_FAILURE)
	{
		fprintf(stderr, "Try '%s generate -h' for more informations.\n", prg_name);
	}
	else
	{
		printf("Usage: %s generate [-h] [-n NETWORK] [-f FORMAT]\n", prg_name);
		printf("Flags:\n");
		printf("\t-h\tdisplay this help and exit.\n");
		printf("Options:\n");
		printf("\t-n NETWORK\tset network address format (default: %s)\n", network);
		printf("\t-f FORMAT\tset output format (default: %s)\n", format);
		printf("\t-k KEYSTORE\t(default: %s)\n", keystore);
	}

	exit(retval);
}

int
generate_main(int argc, char *argv[])
{
	const char *mnemonic[DUNITER_MNEMONIC_WORDS];
	uint8_t seed[DUNITER_SEED_BYTES];
	char seed_encoded[DUNITER_SEED_BYTES * 2 + 1];
	struct duniter_keypair kp;
	char pubkey_encoded[DUNITER_PUBLICKEY_BYTES * 2 + 1];
	char address[DUNITER_ADDRESS_LENGTH];
	char legacy[DUNITER_LEGACY_ADDRESS_WITH_CHKSUM_LENGTH];
	FILE *fp;

	keystore = path;

	while ((argc > 1) && (argv[1][0] == '-'))
	{
		switch (argv[1][1])
		{
			case 'h':
				generate_usage(EXIT_SUCCESS);
				break;
			case 'n':
				argc--;
				argv++;
				if (argc < 2) generate_usage(EXIT_FAILURE);
				network = argv[1];
				break;
			case 'f':
				argc--;
				argv++;
				if (argc < 2) generate_usage(EXIT_FAILURE);
				format = argv[1];
				break;
			case 'k':
				argc--;
				argv++;
				if (argc < 2) generate_usage(EXIT_FAILURE);
				keystore = argv[1];
				break;
			default:
				generate_usage(EXIT_FAILURE);
				break;
		}

		argc--;
		argv++;
	}
	set_prefix();

	if (duniter_initialize() != DUNITER_OK)
	{
		fprintf(stderr, "Can't initialize library\n");
		return (EXIT_FAILURE);
	}

	if (duniter_mnemonic_generate(mnemonic, DUNITER_MNEMONIC_ENGLISH) != DUNITER_OK)
	{
		fprintf(stderr, "Can't generate mnemonic\n");
		return (EXIT_FAILURE);
	}

	if (duniter_mnemonic_to_seed(seed, DUNITER_SEED_BYTES, mnemonic, "") != DUNITER_OK)
	{
		fprintf(stderr, "Can't generate seed\n");
		return (EXIT_FAILURE);
	}
	sodium_bin2hex(seed_encoded, DUNITER_SEED_BYTES * 2 + 1, seed, DUNITER_SEED_BYTES);

	if (duniter_keypair_from_seed(&kp, seed, DUNITER_SR25519) != DUNITER_OK)
	{
		fprintf(stderr, "Can't generate keypair\n");
		return (EXIT_FAILURE);
	}
	sodium_bin2hex(pubkey_encoded, DUNITER_PUBLICKEY_BYTES * 2 + 1, kp.pk, DUNITER_PUBLICKEY_BYTES);

	if (duniter_address_from_pubkey(address, prefix, kp.pk) != DUNITER_OK)
	{
		fprintf(stderr, "Can't generate address\n");
		return (EXIT_FAILURE);
	}

	if (duniter_legacy_address_from_pubkey(legacy, DUNITER_LEGACY_ADDRESS_WITH_CHKSUM_LENGTH, kp.pk) != DUNITER_OK)
	{
		fprintf(stderr, "Can't generate legacy address\n");
		return (EXIT_FAILURE);
	}

	if (strcasecmp(format, "json") == 0)
	{
		/* TODO */
		return (EXIT_FAILURE);
	}
	else
	{
		printf("Secret phrase: `");
		display_mnemonic(mnemonic);
		printf("`\n");

		printf("Secret seed:    %s\n", seed_encoded);
		printf("Public key:     %s\n", pubkey_encoded);
		printf("SS58 address:   %s\n", address);
		printf("Legacy address: %s\n", legacy);
	}

	/* */
	if (access(keystore, F_OK) == 0)
	{
		printf("\n%s exist do you want to overwrite it ? (y/N)", keystore);
		if (fgetc(stdin) != 'y') return (EXIT_FAILURE);
	}

	fp = fopen(keystore, "wb");
	if (fp == NULL)
	{
		fprintf(stderr, "%s: %s\n", keystore, strerror(errno));
		return (EXIT_FAILURE);
	}

	if (fwrite(seed, 1, DUNITER_SEED_BYTES, fp) != DUNITER_SEED_BYTES)
	{
		fprintf(stderr, "Can't save seed\n");
		return (EXIT_FAILURE);
	}

	fclose(fp);

	return (EXIT_SUCCESS);
}