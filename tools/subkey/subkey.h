#ifndef LIBDUNITER_TOOLS_SUBKEY_H
# define LIBDUNITER_TOOLS_SUBKEY_H 1

struct subkey_command {
	const char *name;
	int (*cmd_main)(int argc, char *argv[]);
	const char *help_str;
};

int generate_main(int argc, char *argv[]);
int inspect_main(int argc, char *argv[]);
int sign_main(int argc, char *argv[]);
int verify_main(int argc, char *argv[]);

extern const char *prg_name;
extern char path[1024];

#endif /* !LIBDUNITER_TOOLS_SUBKEY_H */