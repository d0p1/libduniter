#include <sodium/crypto_sign_ed25519.h>
#include <sodium/utils.h>
#include <stdlib.h>
#include <stdio.h>
#include <duniter.h>
#include <string.h>
#include <errno.h>
#include <sodium.h>
#include "duniter/account/keypair.h"
#include "subkey.h"

static const char *keystore = NULL;
static const char *network = "gdev";
static uint16_t prefix = DUNITER_SS58_GDEV_PREFIX;

static inline void
inspect_usage(int retval)
{
	if (retval == EXIT_FAILURE)
	{
		fprintf(stderr, "Try '%s inspect -h' for more information.\n", prg_name);
	}
	else
	{
		printf("Usage: %s inspect [-h] [-k keystore] [-n network] [pubkey] [mnemonic]\n", prg_name);
		printf("Flags:\n");
		printf("\t-h\tdisplay this help and exit.\n");
		printf("Options:\n");
		printf("\t-n NETWORK\tset network address format (default: %s)\n", network);
		printf("\t-k KEYSTORE\t(default: %s)\n", keystore);
	}

	exit(retval);
}


static inline void
set_prefix(void)
{
	if (strcasecmp(network, "g1") == 0)
	{
		prefix = DUNITER_SS58_G1_PREFIX;
	}
	else if (strcasecmp(network, "gdev") == 0)
	{
		prefix = DUNITER_SS58_GDEV_PREFIX;
	}
	else if (strcasecmp(network, "gtest") == 0)
	{
		prefix = DUNITER_SS58_GTEST_PREFIX;
	}
	else
	{
		fprintf(stderr, "%s: unknown network, default to gdev\n", network);
	}
}

int
inspect_main(int argc, char *argv[])
{
	uint8_t seed[DUNITER_SEED_BYTES];
	struct duniter_keypair kp;
	char pubkey_encoded[DUNITER_PUBLICKEY_BYTES * 2 + 1];
	char address[DUNITER_ADDRESS_LENGTH];
	char legacy[DUNITER_LEGACY_ADDRESS_WITH_CHKSUM_LENGTH];
	FILE *fp;

	keystore = path;

	while ((argc > 1) && (argv[1][0] == '-'))
	{
		switch (argv[1][1])
		{
			case 'h':
				inspect_usage(EXIT_SUCCESS);
				break;
			case 'n':
				argc--;
				argv++;
				if (argc < 2) inspect_usage(EXIT_FAILURE);
				network = argv[1];
				break;
			case 'k':
				argc--;
				argv++;
				if (argc < 2) inspect_usage(EXIT_FAILURE);
				keystore = argv[1];
				break;
		}
		argv++;
		argc--;
	}
	set_prefix();

	if (argc < 2)
	{
		fp = fopen(keystore, "rb");
		if (fp == NULL)
		{
			fprintf(stderr, "%s: %s\n", keystore, strerror(errno));
			return (EXIT_FAILURE);
		}

		if (fread(seed, 1, DUNITER_SEED_BYTES, fp) != DUNITER_SEED_BYTES)
		{
			fprintf(stderr, "Can't read seed\n");
			return (EXIT_FAILURE);
		}

		fclose(fp);

		if (duniter_keypair_from_seed(&kp, seed, DUNITER_SR25519) != DUNITER_OK)
		{
			fprintf(stderr, "Can't generate keypair\n");
			return (EXIT_FAILURE);
		}
		sodium_bin2hex(pubkey_encoded, DUNITER_PUBLICKEY_BYTES * 2 + 1, kp.pk, DUNITER_PUBLICKEY_BYTES);

		if (duniter_address_from_pubkey(address, prefix, kp.pk) != DUNITER_OK)
		{
			fprintf(stderr, "Can't generate address\n");
			return (EXIT_FAILURE);
		}

		if (duniter_legacy_address_from_pubkey(legacy, DUNITER_LEGACY_ADDRESS_WITH_CHKSUM_LENGTH, kp.pk) != DUNITER_OK)
		{
			fprintf(stderr, "Can't generate legacy address\n");
			return (EXIT_FAILURE);
		}
	}

	printf("Public key:     %s\n", pubkey_encoded);
	printf("SS58 address:   %s\n", address);
	printf("Legacy address: %s\n", legacy);

	return (EXIT_SUCCESS);
}