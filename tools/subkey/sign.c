#include <sodium/crypto_sign.h>
#include <sodium/utils.h>
#include <stdlib.h>
#include <stdio.h>
#include <duniter.h>
#include <string.h>
#include <errno.h>
#include "duniter/account/keypair.h"
#include "subkey.h"
#ifdef WIN32
# include <io.h>
# define F_OK 0
# define access _access
#else
# include <unistd.h>
#endif
#include <sodium.h>


static const char *keystore = NULL;
static const char *message = NULL;

static inline void
sign_usage(int retval)
{
	if (retval == EXIT_FAILURE)
	{
		fprintf(stderr, "Try '%s sign -h' for more information.\n", prg_name);
	}
	else
	{
		printf("Usage: %s sign [-h]\n", prg_name);
		printf("Flags:\n");
		printf("\t-h\tdisplay this help and exit.\n");
		printf("Options\n");
		printf("\t-k KEYSTORE\t(default: %s)\n", keystore);
	}

	exit(retval);
}

int
sign_main(int argc, char *argv[])
{
	keystore = path;
	FILE *fp;
	uint8_t seed[DUNITER_SEED_BYTES];
	uint8_t sig[crypto_sign_BYTES];
	char sig_encoded[crypto_sign_BYTES * 2 + 1];
	struct duniter_keypair kp;

	while ((argc > 1) && (argv[1][0] == '-'))
	{
		switch (argv[1][1])
		{
			case 'h':
				sign_usage(EXIT_SUCCESS);
				break;
			case 'k':
				argc--;
				argv++;
				if (argc < 2) sign_usage(EXIT_FAILURE);
				keystore = argv[1];
				break;
			case 'm':
				argc--;
				argv++;
				if (argc < 2) sign_usage(EXIT_FAILURE);
				message = argv[1];
				break;
			default:
				sign_usage(EXIT_FAILURE);
				break;
		}

		argc--;
		argv++;
	}

	if (access(keystore, F_OK) != 0)
	{
		fprintf(stderr, "%s: Doesn't exist\n", keystore);
		return (EXIT_FAILURE);
	}

	fp = fopen(keystore, "rb");
	if (fp == NULL)
	{
		fprintf(stderr, "%s: %s\n", keystore, strerror(errno));
		return (EXIT_FAILURE);
	}

	if (fread(seed, 1, DUNITER_SEED_BYTES, fp) != DUNITER_SEED_BYTES)
	{
		return (EXIT_FAILURE);
	}

	fclose(fp);


	if (duniter_keypair_from_seed(&kp, seed, DUNITER_SR25519) != DUNITER_OK)
	{
		return (EXIT_FAILURE);
	}

	if (message == NULL)
	{
		printf("No message suplied\n");
		return (EXIT_FAILURE);
	}

	crypto_sign_detached(sig, NULL, (const uint8_t *)message, strlen(message), kp.sk);

	printf("%s\n", sodium_bin2hex(sig_encoded, crypto_sign_BYTES * 2 + 1, sig, crypto_sign_BYTES));


	return (EXIT_SUCCESS);
}