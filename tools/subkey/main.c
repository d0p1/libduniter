#include <stdlib.h>
#include <stdio.h>
#include <duniter.h>
#include <string.h>
#include "subkey.h"
#include "config.h"
#ifdef _WIN32
# include <shlobj.h>
# include <direct.h>
#else
# include <sys/stat.h>
#endif /* _WIN32 */
#ifdef HAVE_LIBGEN_H
# include <libgen.h>
#endif /* HAVE_LIBGEN_H */
#include "gettext.h"

#define _(STRING) gettext(STRING)

const char *prg_name;
char path[1024];
static const struct subkey_command commands[] = {
	{"generate", generate_main, "generate public and private key."},
	{"inspect", inspect_main, ""},
	{"sign", sign_main, ""},
	{"verify", verify_main, ""},
	{NULL, NULL, NULL},
};

static inline void
version(void)
{
	printf("%s (%s) %s\n", prg_name, PACKAGE_NAME, PACKAGE_VERSION);
}

static inline void
usage(int retval)
{
	size_t idx;

	if (retval == EXIT_FAILURE)
	{
		fprintf(stderr, _("Try '%s -h' for more informations.\n"), prg_name);
	}
	else
	{
		printf("Usage: %s [-hV]\n", prg_name);
		printf(_("Flags:\n"));
		printf(_("\t-h\tdisplay this help and exit.\n"));
		printf(_("\t-V\toutput version information.\n"));
		printf(_("Commands\n"));
		for (idx = 0; commands[idx].name != NULL; idx++)
		{
			printf("\t%-12s\t%s\n", commands[idx].name, commands[idx].help_str);
		}

		printf(_("\nReport bugs to <%s>\n"), PACKAGE_BUGREPORT);
	}

	exit(retval);
}

int
main(int argc, char *argv[])
{
	size_t idx;

#ifdef HAVE_LIBGEN_H
	prg_name = basename(argv[0]);
#else
	prg_name = argv[0];
#endif

#if defined ENABLE_NLS && ENABLE_NLS
	setlocale(LC_ALL, "");
	bindtextdomain(PACKAGE, LOCALEDIR);
	textdomain(PACKAGE);
#endif /* defined ENABLE_NLS && ENABLE_NLS */

#ifdef _WIN32
	SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, path);
	strcat(path, "\\.duniter");
	mkdir(path);
	strcat(path, "\\keystore");
#else 
	if (getenv("HOME") != NULL)
	{
		strcpy(path, getenv("HOME"));
	}
	strcat(path, "/.duniter");
	mkdir(path, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	strcat(path, "/keystore");
#endif

	while ((argc > 1) && (argv[1][0]) == '-')
	{
		switch (argv[1][1])
		{
			case 'h':
				usage(EXIT_SUCCESS);
				break;

			case 'V':
				version();
				break;

			default:
				usage(EXIT_FAILURE);
				break;
		}

		argc--;
		argv++;
	}

	if (argc < 2)
	{
		usage(EXIT_FAILURE);
	}

	for (idx = 0; commands[idx].name != NULL; idx++)
	{
		if (strcasecmp(commands[idx].name, argv[1]) == 0)
		{
			return (commands[idx].cmd_main(argc - 1, argv + 1));
		}
	}

	return (EXIT_FAILURE);
}