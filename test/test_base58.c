#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <setjmp.h>
#include <cmocka.h>
#include <string.h>
#include <stdio.h>
#include <duniter/codec/base58.h>

/* test vector from https://digitalbazaar.github.io/base58-spec/ */

const char *test_case1_data = "Hello World!";
const char *test_case1_b58 = "2NEpo7TZRRrLZSi2U";

const char *test_case2_data = "The quick brown fox jumps over the lazy dog.";
const char *test_case2_b58 = "USm3fpXnKG5EUBx2ndxBDMPVciP5hGey2Jh4NDv6gmeo1LkMeiKrLJUUBk6Z";

static void
test_b58_encode_tc1(void **state)
{
	char encoded[18];
	size_t encoded_len = 18;
	int rc;

	(void)state;

	rc = duniter_base58_encode(encoded, &encoded_len, test_case1_data, strlen(test_case1_data));

	assert_return_code(rc, 0);
	assert_string_equal(encoded, test_case1_b58);
}

static void
test_b58_encode_tc2(void **state)
{
	char encoded[61];
	size_t encoded_len = 61;
	int rc;

	(void)state;

	rc = duniter_base58_encode(encoded, &encoded_len, test_case2_data, strlen(test_case2_data));

	assert_return_code(rc, 0);
	assert_string_equal(encoded, test_case2_b58);
}


int
main(void)
{
	const struct CMUnitTest tests[] = {
		cmocka_unit_test(test_b58_encode_tc1),
		cmocka_unit_test(test_b58_encode_tc2),
		
	};
 
	return cmocka_run_group_tests(tests, NULL, NULL);
}