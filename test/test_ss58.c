#include "duniter/account/keypair.h"
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <setjmp.h>
#include <cmocka.h>
#include <string.h>
#include <stdio.h>
#include <duniter.h>

/* test vector from https://docs.substrate.io/reference/address-formats/ */

uint8_t test_case1_data[DUNITER_PUBLICKEY_BYTES] = {
	0xd6, 0xa3, 0x10, 0x5d, 0x67, 0x68, 0xe9, 0x56,
	0xe9, 0xe5, 0xd4, 0x10, 0x50, 0xac, 0x29, 0x84,
	0x3f, 0x98, 0x56, 0x14, 0x10, 0xd3, 0xa4, 0x7f,
	0x9d, 0xd5, 0xb3, 0xb2, 0x27, 0xab, 0x87, 0x46
};
const char *test_case1_ss58 = "5Gv8YYFu8H1btvmrJy9FjjAWfb99wrhV3uhPFoNEr918utyR";

uint8_t test_case2_data[DUNITER_PUBLICKEY_BYTES] = {
	0x46, 0xeb, 0xdd, 0xef, 0x8c, 0xd9, 0xbb, 0x16,
	0x7d, 0xc3, 0x08, 0x78, 0xd7, 0x11, 0x3b, 0x7e,
	0x16, 0x8e, 0x6f, 0x06, 0x46, 0xbe, 0xff, 0xd7,
	0x7d, 0x69, 0xd3, 0x9b, 0xad, 0x76, 0xb4, 0x7a
};
const char *test_case2_ss58 = "12bzRJfh7arnnfPPUZHeJUaE62QLEwhK48QnH9LXeK2m1iZU";

static void
test_ss58_encode_pubkey_tc1(void **state)
{
	char encoded[49];
	size_t encoded_len = 49;
	int rc;

	(void)state;

	rc = duniter_ss58_encode(encoded, &encoded_len, test_case1_data, DUNITER_PUBLICKEY_BYTES, 42);

	assert_return_code(rc, 0);
	assert_string_equal(encoded, test_case1_ss58);
}

static void
test_ss58_encode_pubkey_tc2(void **state)
{
	char encoded[49];
	size_t encoded_len = 49;
	int rc;

	(void)state;

	rc = duniter_ss58_encode(encoded, &encoded_len, test_case2_data, DUNITER_PUBLICKEY_BYTES, 0);

	assert_return_code(rc, 0);
	assert_string_equal(encoded, test_case2_ss58);
}



int
main(void)
{
	const struct CMUnitTest tests[] = {
		cmocka_unit_test(test_ss58_encode_pubkey_tc1),
		cmocka_unit_test(test_ss58_encode_pubkey_tc2),
		
	};
 
	return cmocka_run_group_tests(tests, NULL, NULL);
}