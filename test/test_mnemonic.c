#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <setjmp.h>
#include <cmocka.h>
#include <stdio.h>

#include <duniter/account/mnemonic.h>

/* tests vectors from:
 * https://github.com/trezor/python-mnemonic/blob/master/vectors.json */

static uint8_t zero_entropy[DUNITER_MNEMONIC_ENTROPY_BYTES] = {
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};
static char *zero_entropy_mnemonic[DUNITER_MNEMONIC_WORDS] = {
	"abandon", "abandon", "abandon", "abandon", "abandon", "abandon",
	"abandon", "abandon", "abandon", "abandon", "abandon", "about"
};
static char *zero_entropy_french_mnemonic[DUNITER_MNEMONIC_WORDS] = {
	"abaisser", "abaisser", "abaisser", "abaisser", "abaisser", "abaisser",
	"abaisser", "abaisser", "abaisser", "abaisser", "abaisser", "abeille"
};
static char *zero_entropy_spanish_mnemonic[DUNITER_MNEMONIC_WORDS] = {
	"ábaco", "ábaco", "ábaco", "ábaco", "ábaco", "ábaco",
	"ábaco", "ábaco", "ábaco", "ábaco", "ábaco", "abierto"
};
static uint8_t zero_entropy_seed[64U] = {
	0xc5, 0x52, 0x57, 0xc3, 0x60, 0xc0, 0x7c, 0x72,
	0x02, 0x9a, 0xeb, 0xc1, 0xb5, 0x3c, 0x05, 0xed,
	0x03, 0x62, 0xad, 0xa3, 0x8e, 0xad, 0x3e, 0x3e,
	0x9e, 0xfa, 0x37, 0x08, 0xe5, 0x34, 0x95, 0x53,
	0x1f, 0x09, 0xa6, 0x98, 0x75, 0x99, 0xd1, 0x82,
	0x64, 0xc1, 0xe1, 0xc9, 0x2f, 0x2c, 0xf1, 0x41,
	0x63, 0x0c, 0x7a, 0x3c, 0x4a, 0xb7, 0xc8, 0x1b,
	0x2f, 0x00, 0x16, 0x98, 0xe7, 0x46, 0x3b, 0x04
};

static uint8_t ff_entropy[DUNITER_MNEMONIC_ENTROPY_BYTES] = {
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
};
static char *ff_entropy_mnemonic[DUNITER_MNEMONIC_WORDS] = {
	"zoo", "zoo", "zoo", "zoo", "zoo", "zoo",
	"zoo", "zoo", "zoo", "zoo", "zoo", "wrong"
};
static char *ff_entropy_french_mnemonic[DUNITER_MNEMONIC_WORDS] = {
	"zoologie", "zoologie", "zoologie", "zoologie", "zoologie", "zoologie",
	"zoologie", "zoologie", "zoologie", "zoologie", "zoologie", "voter"
};
static char *ff_entropy_spanish_mnemonic[DUNITER_MNEMONIC_WORDS] = {
	"zurdo", "zurdo", "zurdo", "zurdo", "zurdo", "zurdo",
	"zurdo", "zurdo", "zurdo", "zurdo", "zurdo", "yodo"
};
static uint8_t ff_entropy_seed[64U] = {
	0xac, 0x27, 0x49, 0x54, 0x80, 0x22, 0x52, 0x22,
	0x07, 0x9d, 0x7b, 0xe1, 0x81, 0x58, 0x37, 0x51,
	0xe8, 0x6f, 0x57, 0x10, 0x27, 0xb0, 0x49, 0x7b,
	0x5b, 0x5d, 0x11, 0x21, 0x8e, 0x0a, 0x8a, 0x13,
	0x33, 0x25, 0x72, 0x91, 0x7f, 0x0f, 0x8e, 0x5a,
	0x58, 0x96, 0x20, 0xc6, 0xf1, 0x5b, 0x11, 0xc6, 
	0x1d, 0xee, 0x32, 0x76, 0x51, 0xa1, 0x4c, 0x34,
	0xe1, 0x82, 0x31, 0x05, 0x2e, 0x48, 0xc0, 0x69
};

static uint8_t x7f_entropy[DUNITER_MNEMONIC_ENTROPY_BYTES] = {
	0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f,
	0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f
};
static char *x7f_entropy_mnemonic[DUNITER_MNEMONIC_WORDS] = {
	"legal", "winner", "thank", "year",   "wave",  "sausage", 
	"worth", "useful", "legal", "winner", "thank", "yellow"
};
static char *x7f_entropy_french_mnemonic[DUNITER_MNEMONIC_WORDS] = {
	"implorer", "visage", "sonnette", "voyage", "véloce", "pourpre",
	"volaille", "tribunal", "implorer", "visage", "sonnette", "voyelle"
};
static char *x7f_entropy_spanish_mnemonic[DUNITER_MNEMONIC_WORDS] = {
	"ligero", "vista", "talar", "yogur", "venta", "queso",
	"yacer", "trozo", "ligero", "vista", "talar", "zafiro"
};


static void
test_generate_with_know_entropy(void **state)
{
	const char *mnemonic[DUNITER_MNEMONIC_WORDS];
	int i;

	(void)state;

	duniter_mnemonic_from_entropy(mnemonic, DUNITER_MNEMONIC_ENGLISH, zero_entropy);
	for (i = 0; i < DUNITER_MNEMONIC_WORDS; i++)
	{
		assert_string_equal(mnemonic[i], zero_entropy_mnemonic[i]);
	}

	duniter_mnemonic_from_entropy(mnemonic, DUNITER_MNEMONIC_ENGLISH, ff_entropy);
	for (i = 0; i < DUNITER_MNEMONIC_WORDS; i++)
	{
		assert_string_equal(mnemonic[i], ff_entropy_mnemonic[i]);
	}

	duniter_mnemonic_from_entropy(mnemonic, DUNITER_MNEMONIC_ENGLISH, x7f_entropy);
	for (i = 0; i < DUNITER_MNEMONIC_WORDS; i++)
	{
		assert_string_equal(mnemonic[i], x7f_entropy_mnemonic[i]);
	}
}

static void
test_generate_french_with_know_entropy(void **state)
{
	const char *mnemonic[DUNITER_MNEMONIC_WORDS];
	int i;

	(void)state;

	duniter_mnemonic_from_entropy(mnemonic, DUNITER_MNEMONIC_FRENCH, zero_entropy);
	for (i = 0; i < DUNITER_MNEMONIC_WORDS; i++)
	{
		assert_string_equal(mnemonic[i], zero_entropy_french_mnemonic[i]);
	}

	duniter_mnemonic_from_entropy(mnemonic, DUNITER_MNEMONIC_FRENCH, ff_entropy);
	for (i = 0; i < DUNITER_MNEMONIC_WORDS; i++)
	{
		assert_string_equal(mnemonic[i], ff_entropy_french_mnemonic[i]);
	}

	duniter_mnemonic_from_entropy(mnemonic, DUNITER_MNEMONIC_FRENCH, x7f_entropy);
	for (i = 0; i < DUNITER_MNEMONIC_WORDS; i++)
	{
		assert_string_equal(mnemonic[i], x7f_entropy_french_mnemonic[i]);
	}
}

static void
test_generate_spanish_with_know_entropy(void **state)
{
	const char *mnemonic[DUNITER_MNEMONIC_WORDS];
	int i;

	(void)state;

	duniter_mnemonic_from_entropy(mnemonic, DUNITER_MNEMONIC_SPANISH, zero_entropy);
	for (i = 0; i < DUNITER_MNEMONIC_WORDS; i++)
	{
		assert_string_equal(mnemonic[i], zero_entropy_spanish_mnemonic[i]);
	}

	duniter_mnemonic_from_entropy(mnemonic, DUNITER_MNEMONIC_SPANISH, ff_entropy);
	for (i = 0; i < DUNITER_MNEMONIC_WORDS; i++)
	{
		assert_string_equal(mnemonic[i], ff_entropy_spanish_mnemonic[i]);
	}

	duniter_mnemonic_from_entropy(mnemonic, DUNITER_MNEMONIC_SPANISH, x7f_entropy);
	for (i = 0; i < DUNITER_MNEMONIC_WORDS; i++)
	{
		assert_string_equal(mnemonic[i], x7f_entropy_spanish_mnemonic[i]);
	}
}

static void
test_seed_with_know_entropy(void **state)
{
	const char *mnemonic[DUNITER_MNEMONIC_WORDS];
	uint8_t seed[64];

	(void)state;

	duniter_mnemonic_from_entropy(mnemonic, DUNITER_MNEMONIC_ENGLISH, zero_entropy);
	duniter_mnemonic_to_seed(seed, 64, mnemonic, "TREZOR");

	assert_memory_equal(seed, zero_entropy_seed, DUNITER_SEED_BYTES);

	
	duniter_mnemonic_from_entropy(mnemonic, DUNITER_MNEMONIC_ENGLISH, ff_entropy);
	duniter_mnemonic_to_seed(seed, 64, mnemonic, "TREZOR");

	assert_memory_equal(seed, ff_entropy_seed, DUNITER_SEED_BYTES);
}

int
main(void)
{
	const struct CMUnitTest tests[] = {
		cmocka_unit_test(test_generate_with_know_entropy),
		cmocka_unit_test(test_generate_french_with_know_entropy),
		cmocka_unit_test(test_generate_spanish_with_know_entropy),

		cmocka_unit_test(test_seed_with_know_entropy),
	};
 
	return cmocka_run_group_tests(tests, NULL, NULL);
}