#include <stddef.h>
#include <duniter.h>

const char *
duniter_strerror(enum duniter_error err)
{
	switch (err)
	{
		case DUNITER_OK:
			return "OK";

		case DUNITER_ERR_NOT_IMPLEM:
			return "Not implemented";
		
		default:
			return "???";
	}

	/* unreachable */
	return (NULL);
}