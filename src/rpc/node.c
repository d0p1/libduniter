#include <stddef.h>
#include <curl/curl.h>
#include <curl/easy.h>
#include <curl/websockets.h>
#include <duniter.h>

int
duniter_node_connect(struct duniter_node *node, const char *url)
{
	CURLcode res;

	if (node == NULL)
	{
		return (-1);
	}

	node->curl = curl_easy_init();

	if (node->curl == NULL)
	{
		return (-1);
	}

	curl_easy_setopt(node->curl,CURLOPT_URL, url);

	curl_easy_setopt(node->curl, CURLOPT_CONNECT_ONLY, 2L);

	res = curl_easy_perform(node->curl);
	return (res == CURLE_OK);	
}

void
duniter_node_close(struct duniter_node *node)
{
	size_t sent;

	if (node == NULL)
	{
		return;
	}

	curl_ws_send(node->curl, "", 0, &sent, 0, CURLWS_CLOSE);

	curl_easy_cleanup(node->curl);
}

void
duniter_rpc_loop(struct duniter_node *node)
{
	size_t rlen;
	struct curl_ws_frame *meta;
	char buff[1024] = { 0 };

	while (1)
	{
		curl_ws_recv(node->curl, buff, 1024, &rlen, &meta);
		printf("%s\n", buff);
	}
}