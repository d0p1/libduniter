
#include <string.h>
#include <sodium.h>
#include <duniter.h>
#include "crypto.h"

static const char *const MNEMONIC_ENGLISH[2048] = {
#include "data/english.inc"
};

static const char *const MNEMONIC_FRENCH[2048] = {
#include "data/french.inc"
};

static const char *const MNEMONIC_SPANISH[2048] = {
#include "data/spanish.inc"
};

static const char *
mnemonic_get_from_lang(enum duniter_mnemonic_lang lang, int idx)
{
	switch (lang)
	{
		case DUNITER_MNEMONIC_LANG_MAX:
		case DUNITER_MNEMONIC_ENGLISH:
			return MNEMONIC_ENGLISH[idx];
		case DUNITER_MNEMONIC_FRENCH:
			return MNEMONIC_FRENCH[idx];
		case DUNITER_MNEMONIC_SPANISH:
			return MNEMONIC_SPANISH[idx];
	}

	return (MNEMONIC_ENGLISH[idx]);
}

int
duniter_mnemonic_from_entropy(const char *mnemonic[DUNITER_MNEMONIC_WORDS], enum duniter_mnemonic_lang lang, uint8_t *entropy)
{
	int idx;
	int i, j;
	uint8_t hash[crypto_hash_sha256_BYTES];

	if (crypto_hash_sha256(hash, entropy, 16) != 0)
	{
		return (-1);
	}
	hash[16] = hash[0];

	memcpy(hash, entropy, 16);

	for (i = 0; i < DUNITER_MNEMONIC_WORDS; i++)
	{
		idx = 0;
		for (j = 0; j < 11; j++)
		{
			idx <<= 1;
			idx += (hash[(i * 11 + j) / 8] & (1 << (7 - ((i * 11 + j) % 8)))) > 0;
		}

		mnemonic[i] = mnemonic_get_from_lang(lang, idx);
	}
	return (DUNITER_OK);
}

int
duniter_mnemonic_generate(const char *mnemonic[DUNITER_MNEMONIC_WORDS], enum duniter_mnemonic_lang lang)
{
	uint8_t random_data[32];

	randombytes_buf(random_data, 32);

	return (duniter_mnemonic_from_entropy(mnemonic, lang, random_data));
}

int
duniter_mnemonic_to_seed(uint8_t *seed, size_t size, const char *const *mnemonic, const char *pass)
{
	uint8_t key[256] = { 0 };
	uint8_t salt[256 + 8];
	size_t passlen;
	int i;

	memcpy(salt, "mnemonic", 8);
	passlen = strlen(pass);
	if (passlen > 256)
	{
		passlen = 256;
	}
	memcpy(salt + 8, pass, passlen);

	for (i = 0; i < DUNITER_MNEMONIC_WORDS; i++)
	{
		strcat((char *)key, mnemonic[i]);
		if (i < 11)
		{
			strcat((char *)key, " ");
		}
	}

	fastpbkdf2_hmac_sha512(key, strlen((char *)key), salt, passlen + 8, 2048, seed, size);
	return (DUNITER_OK);
}