#include <string.h>
#include <duniter.h>
#include <sodium.h>
#include "crypto.h"

int
duniter_keypair_from_seed(struct duniter_keypair *kp, const uint8_t seed[DUNITER_SEED_BYTES],
	enum duniter_keypair_type type)
{
	if (kp == NULL || seed == NULL)
	{
		return (-1);
	}

	kp->type = type;

	if (type == DUNITER_ED25519) return (crypto_sign_seed_keypair(kp->pk, kp->sk, seed));

	return (duniter_sr25519_from_seed(kp, seed));
}

int
duniter_keypair_from_sk(struct duniter_keypair *kp, const uint8_t sk[DUNITER_SECRETKEY_BYTES],
	enum duniter_keypair_type type)
{
	if (kp == NULL || sk == NULL)
	{
		return (-1);
	}

	if (sk != kp->sk)
	{
		memcpy(kp->sk, sk, DUNITER_SECRETKEY_BYTES);
	}

	kp->type = type;

	if (type == DUNITER_ED25519) return (crypto_sign_ed25519_sk_to_pk(kp->pk, kp->sk));

	return (duniter_sr25519_from_sk(kp, sk));
}
