#include <duniter.h>

int
duniter_address_from_pubkey(char *address, uint16_t network, uint8_t pk[DUNITER_PUBLICKEY_BYTES])
{
	size_t sz;

	sz = DUNITER_ADDRESS_LENGTH;

	return (duniter_ss58_encode(address, &sz, pk, DUNITER_PUBLICKEY_BYTES, network));
}
