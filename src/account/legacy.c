#include <stddef.h>
#include <duniter.h>
#include <string.h>
#include <sodium.h>
#include <duniter.h>

int
duniter_legacy_seed_from_passphrase(uint8_t seed[DUNITER_SEED_BYTES], const char *password, const char *salt)
{
	if (password == NULL || salt == NULL || seed == NULL)
	{
		return (-1);
	}

	return (crypto_pwhash_scryptsalsa208sha256_ll((uint8_t *)password, strlen(password),
		(uint8_t *)salt, strlen(salt), 4096, 16, 1, seed, DUNITER_SEED_BYTES));
}

int
duniter_legacy_address_from_pubkey(char *out, size_t outsz, uint8_t pk[DUNITER_PUBLICKEY_BYTES])
{
	uint8_t hash_root[crypto_hash_sha256_BYTES];
	uint8_t hash_squared[crypto_hash_sha256_BYTES];
	char encoded_chksum[45];
	size_t encode_len;

	if (crypto_hash_sha256(hash_root, pk, DUNITER_PUBLICKEY_BYTES) != 0)
	{
		return (-1);
	}

	if (crypto_hash_sha256(hash_squared, hash_root, crypto_hash_sha256_BYTES) != 0)
	{
		return (-1);
	}

	encode_len = outsz;
	if (duniter_base58_encode(out, &encode_len, pk, DUNITER_PUBLICKEY_BYTES) != DUNITER_OK)
	{
		return (-1);
	}

	if (outsz != DUNITER_LEGACY_ADDRESS_WITH_CHKSUM_LENGTH)
	{
		return (DUNITER_OK);
	}

	outsz = encode_len;
	encode_len = 45;
	if (duniter_base58_encode(encoded_chksum, &encode_len, hash_squared, crypto_hash_sha256_BYTES))
	{
		return (-1);
	}

	out[outsz - 1] = ':';
	out[outsz] = encoded_chksum[0];
	out[outsz + 1] = encoded_chksum[1];
	out[outsz + 2] = encoded_chksum[2];
	out[outsz + 3] = '\0';

	return (DUNITER_OK);
}

/* TODO: decode legacy address and ensure checksum match */
int
duniter_legacy_address_to_pubkey(uint8_t pk[DUNITER_PUBLICKEY_BYTES], const char *address)
{
	(void)pk;
	(void)address;

	return (DUNITER_ERR_NOT_IMPLEM);
}