#include <sodium/crypto_core_ristretto255.h>
#include <sodium/crypto_scalarmult_ristretto255.h>
#include <string.h>
#include <sodium.h>
#include <sodium/crypto_hash_sha512.h>
#include "duniter/account/keypair.h"
#include "duniter/error.h"
#include "sr25519.h"

/* ----------------------------------------------------------------------------
 * original code by Terence Ge, licensed under Apache 2.0
 * url: https://github.com/TerenceGe/sr25519-donna/blob/master/src/sr25519.c
 */

static void
divide_scalar_bytes_by_cofactor(uint8_t *scalar, size_t scalar_len) {
	int i;
	uint8_t low;

	low = 0;
	for (i = scalar_len - 1; i >= 0; i--)
	{
		uint8_t r = scalar[i] & 7;
		scalar[i] >>= 3;
		scalar[i] += low;
		low = r << 5;
	}
}

static void
expand_ed25519(uint8_t key[DUNITER_SECRETKEY_BYTES],
				const uint8_t seed[DUNITER_SEED_BYTES]) {
	uint8_t hash[crypto_hash_sha512_BYTES];

	memset(key, 0, DUNITER_SECRETKEY_BYTES);

	crypto_hash_sha512(hash, seed, DUNITER_SEED_BYTES);
	memcpy(key, hash, DUNITER_SECRETKEY_BYTES);
	key[0]  &= 248;
	key[31] &= 63;
	key[31] |= 64;

	divide_scalar_bytes_by_cofactor(key, 32);

	key[31] &= 0b1111111;
}

/* end of sr25519.c 
 * ----------------------------------------------------------------------------
 */


int
duniter_sr25519_from_sk(struct duniter_keypair *kp, const uint8_t sk[DUNITER_SECRETKEY_BYTES])
{
	if (sk != kp->sk)
	{
		memcpy(kp->sk, sk, DUNITER_SECRETKEY_BYTES);
	}

	crypto_scalarmult_ristretto255_base(kp->pk, sk);

	kp->type = DUNITER_SR25519;

	return (DUNITER_OK);
}

int
duniter_sr25519_from_seed(struct duniter_keypair *kp, const uint8_t seed[DUNITER_SEED_BYTES])
{
	expand_ed25519(kp->sk, seed);

	return (duniter_sr25519_from_sk(kp, kp->sk));
}
