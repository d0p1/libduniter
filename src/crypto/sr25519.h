#ifndef SR25519_H
# define SR25519_H 1

# include <duniter.h>

# define DUNITER_SR25519_SECRETNONCE_BYTES 32

int duniter_sr25519_from_seed(struct duniter_keypair *kp, const uint8_t seed[DUNITER_SEED_BYTES]);
int duniter_sr25519_from_sk(struct duniter_keypair *kp, const uint8_t sk[DUNITER_SECRETKEY_BYTES]);

#endif /* !SR25519_H */