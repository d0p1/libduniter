#ifndef DUNITER_INTERNAL_CRYPTO_H
# define DUNITER_INTERNAL_CRYPTO_H 1

# include <stddef.h>
# include <stdint.h>

# include <crypto/fastpbkdf2.h>
# include <crypto/sr25519.h>

#endif /* !DUNITER_INTERNAL_CRYPTO_H */