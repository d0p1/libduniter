#include "duniter/account/keypair.h"
#include "duniter/codec/base58.h"
#include "duniter/error.h"
#include <sodium/crypto_generichash.h>
#include <sodium/crypto_generichash_blake2b.h>
#include <stddef.h>
#include <duniter.h>
#include <string.h>

int
duniter_ss58_encode(char *out, size_t *outsz,
					uint8_t *in, uint8_t insz, uint16_t prefix)
{
	crypto_generichash_state state;
	uint8_t chksum[crypto_generichash_BYTES_MAX];
	uint8_t b58in[DUNITER_PUBLICKEY_BYTES + 1 + 2];

	if (prefix > 16383 || prefix == 46 || prefix == 47)
	{
		return (DUNITER_ERR_SS58_INVALID_PREFIX);
	}

	if (insz != DUNITER_PUBLICKEY_BYTES)
	{
		return (DUNITER_ERR_NOT_IMPLEM);
	}

	if (prefix > 67)
	{
		return (DUNITER_ERR_NOT_IMPLEM);
	}

	b58in[0] = (uint8_t)prefix;
	memcpy(b58in + 1, in, DUNITER_PUBLICKEY_BYTES);

	crypto_generichash_init(&state, NULL, 0, crypto_generichash_BYTES_MAX);
	crypto_generichash_update(&state, (const uint8_t *)"SS58PRE", 7);
	crypto_generichash_update(&state, b58in, DUNITER_PUBLICKEY_BYTES + 1);
	crypto_generichash_final(&state, chksum, crypto_generichash_BYTES_MAX);

	b58in[DUNITER_PUBLICKEY_BYTES + 1] = chksum[0];
	b58in[DUNITER_PUBLICKEY_BYTES + 2] = chksum[1];

	return (duniter_base58_encode(out, outsz, b58in, DUNITER_PUBLICKEY_BYTES + 1 + 2));
}