# libduniter

![Gitlab Pipeline Status](https://img.shields.io/gitlab/pipeline-status/d0p1%2Flibduniter?gitlab_url=https%3A%2F%2Fgit.duniter.org&branch=master&style=flat-square) ![GitLab License](https://img.shields.io/gitlab/license/d0p1%2Flibduniter?gitlab_url=https%3A%2F%2Fgit.duniter.org&style=flat-square)

## Building

```shell
$ autoreconf -i -f
$ ./configure
$ make
$ # run test
$ make check
```

## Donate

`5Y2sMbSfzjXGVou3U1aDgriZUSAoemSTVUsbfVZBTtrx:6X3`

## License

<img src="https://opensource.org/wp-content/themes/osi/assets/img/osi-badge-light.svg" align="right" height="128px" alt="OSI Approved License">

libduniter is licensed under the 3-Clause BSD License.

The full text of the license can be accessed via [this link](https://opensource.org/licenses/BSD-3-Clause) and is also included in [the license file](./LICENSE) of this software package.